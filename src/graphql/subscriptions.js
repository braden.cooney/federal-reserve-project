/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const onCreateTodo = /* GraphQL */ `
  subscription OnCreateTodo {
    onCreateTodo {
      id
      name
      description
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateTodo = /* GraphQL */ `
  subscription OnUpdateTodo {
    onUpdateTodo {
      id
      name
      description
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteTodo = /* GraphQL */ `
  subscription OnDeleteTodo {
    onDeleteTodo {
      id
      name
      description
      createdAt
      updatedAt
    }
  }
`;
export const onCreateForm = /* GraphQL */ `
  subscription OnCreateForm(
    $email: String
    $firstName: String
    $id: ID
    $lastName: String
    $major: String
  ) {
    onCreateForm(
      email: $email
      firstName: $firstName
      id: $id
      lastName: $lastName
      major: $major
    ) {
      email
      firstName
      id
      lastName
      major
      teamName
      tshirt
    }
  }
`;
export const onUpdateForm = /* GraphQL */ `
  subscription OnUpdateForm(
    $email: String
    $firstName: String
    $id: ID
    $lastName: String
    $major: String
  ) {
    onUpdateForm(
      email: $email
      firstName: $firstName
      id: $id
      lastName: $lastName
      major: $major
    ) {
      email
      firstName
      id
      lastName
      major
      teamName
      tshirt
    }
  }
`;
export const onDeleteForm = /* GraphQL */ `
  subscription OnDeleteForm(
    $email: String
    $firstName: String
    $id: ID
    $lastName: String
    $major: String
  ) {
    onDeleteForm(
      email: $email
      firstName: $firstName
      id: $id
      lastName: $lastName
      major: $major
    ) {
      email
      firstName
      id
      lastName
      major
      teamName
      tshirt
    }
  }
`;
