/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const createTodo = /* GraphQL */ `
  mutation CreateTodo(
    $input: CreateTodoInput!
    $condition: ModelTodoConditionInput
  ) {
    createTodo(input: $input, condition: $condition) {
      id
      name
      description
      createdAt
      updatedAt
    }
  }
`;
export const updateTodo = /* GraphQL */ `
  mutation UpdateTodo(
    $input: UpdateTodoInput!
    $condition: ModelTodoConditionInput
  ) {
    updateTodo(input: $input, condition: $condition) {
      id
      name
      description
      createdAt
      updatedAt
    }
  }
`;
export const deleteTodo = /* GraphQL */ `
  mutation DeleteTodo(
    $input: DeleteTodoInput!
    $condition: ModelTodoConditionInput
  ) {
    deleteTodo(input: $input, condition: $condition) {
      id
      name
      description
      createdAt
      updatedAt
    }
  }
`;
export const createForm = /* GraphQL */ `
  mutation CreateForm($input: CreateFormInput!) {
    createForm(input: $input) {
      email
      firstName
      id
      lastName
      major
      teamName
      tshirt
    }
  }
`;
export const updateForm = /* GraphQL */ `
  mutation UpdateForm($input: UpdateFormInput!) {
    updateForm(input: $input) {
      email
      firstName
      id
      lastName
      major
      teamName
      tshirt
    }
  }
`;
export const deleteForm = /* GraphQL */ `
  mutation DeleteForm($input: DeleteFormInput!) {
    deleteForm(input: $input) {
      email
      firstName
      id
      lastName
      major
      teamName
      tshirt
    }
  }
`;
