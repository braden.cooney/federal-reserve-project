import './App.css';
import React, {Component} from 'react';
import Amplify, {Auth, API, graphqlOperation} from 'aws-amplify';
import { withAuthenticator, AmplifySignOut } from '@aws-amplify/ui-react';
import { UserForm } from './Components/UserForm';
import awsconfig from'./aws-exports';
Amplify.configure(awsconfig);
Auth.signIn("Braden", "password");


const App = () => {
    return (
        <div className="App">

            <UserForm />
        </div>
    );
}

export default withAuthenticator(App);
