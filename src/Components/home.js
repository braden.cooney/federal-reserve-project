import React, {Component} from 'react';
import {listForms} from '../graphql/queries.js';
import {createForm} from '../graphql/mutations';
import { API, graphqlOperation} from 'aws-amplify';


export class home extends Component {
    state = {forms: [], firstName: 'Sick', lastName: 'Whatever', email: 'sick@valorant.com', major: 'Valorant', teamName: 'Sentinels', tshirt: 'Medium',id: '1', form: []}
    async componentDidMount() {
        const allForms = await API.graphql({query: listForms});
        const forms = allForms.data.listForms.items;
        console.log(allForms.data.listForms.items);
        this.setState({forms});
    }

    createForm = async () => {
        const {firstName, lastName, email, major, teamName, tshirt, id} = this.state;
        const form = {firstName, lastName, email, major, teamName, tshirt, id}

        await API.graphql(graphqlOperation(createForm, {input: form}))

    }

    render() {
        return (
            <div className = "home">
                {
                    this.state.forms.map((rest, i) => (
                        <div style = {styles.item}>
                            <p style = {styles.firstName}>{rest.firstName}</p>
                            <p style = {styles.lastName}>{rest.lastName}</p>
                        </div>
                    ))
                }
                <button onClick={this.createForm}>Submit</button>

            </div>
        );
    }
}

const styles = {
    item: {
        padding: 10,
        borderBottom: '2px solid #ddd'
    },
    firstName: {fontSize: 22},
    lastName: {color: 'rgba(0,0,0,.45)'}
}

export default home;