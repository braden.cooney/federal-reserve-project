import React, { Component } from 'react';
import FormUserDetails from './FormUserDetails';
import FormUserDetailsTwo from './FormUserDetailsTwo';
import FormUserDetailsThree from './FormUserDetailsThree';
import FormUserDetailsFour from './FormUserDetailsFour';
import {API, graphqlOperation } from 'aws-amplify';
import * as queries from '../graphql/mutations';
import {useState, useEffect} from 'react'
import Confirm from './Confirm';
import Success from './Success';
import { BrowserView, MobileView, isBrowser, isMobile } from 'react';
import Amplify, {Auth} from 'aws-amplify';

Amplify.Logger.LOG_LEVEL = 'Debug';




export class UserForm extends React.Component {
  state = {forms: [],id: '2', form: [],
    step: 1,
    disabled: true,
    firstName: '',
    lastName: '',
    email: '',
    emailError: '',
    teamName: '',
    major: '',
    tShirt: '',
    city: '',
    firstNameTwo: '',
    lastNameTwo: '',
    emailTwo: '',
    teamNameTwo: '',
    majorTwo: '',
    tShirtTwo: '',
    cityTwo: '',
    cityTwoErorr: '',
    firstNameThree: '',
    lastNameThree: '',
    emailThree: '',
    teamNameThree: '',
    majorThree: '',
    tShirtThree: '',
    cityThree: '',
    firstNameFour: '',
    lastNameFour: '',
    emailFour: '',
    emailFourError: '',
    teamNameFour: '',
    majorFour: '',
    tShirtFour: '',
    cityFour: '',
  };


  nextStep = () => {

    const { step } = this.state;
    this.setState({
      step: step + 1
    });
  };

  // Go back to prev step
  prevStep = () => {
    const { step } = this.state;
    this.setState({
      step: step - 1
    });
  };

 
 handleChange = input => e => {
  
    this.setState({ [input]: e.target.value});

    if(this.state.teamName.length >= 3 && this.state.firstName.length >= 3 && this.state.lastName.length >= 3 && this.state.major.length && this.state.tShirt.length >= 3) {
      this.setState({

        disabled: false
      });
    }
   
    else {
        this.setState({
          disabled:true
        });

    }

  };



 saveUser = async () => {
   /*const { firstName, lastName, email, teamName, major, tShirt, city, firstNameTwo,
     lastNameTwo, emailTwo, teamNameTwo, majorTwo, tShirtTwo, cityTwo,
     firstNameThree, lastNameThree,
     emailThree, teamNameThree, majorThree, tShirtThree, cityThree,
     firstNameFour, lastNameFour, emailFour, teamNameFour,
     majorFour, tShirtFour, cityFour} = this.state;*/

   try {
     /*
     const input = {
       firstName: firstName,
       lastName: lastName,
       email: email,
       teamName: teamName,
       major: major,
       tShirt: tShirt,
       city: city,
       firstNameTwo: firstNameTwo,
       lastNameTwo: lastNameTwo,
       emailTwo: emailTwo,
       teamNameTwo: teamNameTwo,
       majorTwo: majorTwo,
       tShirtTwo: tShirtTwo,
       cityTwo: cityTwo,
       firstNameThree: firstNameThree,
       lastNameThree: lastNameThree,
       emailThree: emailThree,
       teamNameThree: teamNameThree,
       majorThree: majorThree,
       tShirtThree: tShirtThree,
       cityThree: cityThree,
       firstNameFour: firstNameFour,
       lastNameFour: lastNameFour,
       emailFour: emailFour,
       teamNameFour: teamNameFour,
       majorFour: majorFour,
       tShirtFour: tShirtFour,
       cityFour: cityFour
     };
     */
     const {firstName, lastName, email, major, teamName, tshirt, id} = this.props;
     const form = {firstName, lastName, email, major, teamName, tshirt, id}
     await API.graphql(graphqlOperation(queries.createForm, form));
     console.log('user created successfully');
   } catch (err) {
     console.log('error', err);
   }

 }

  
  
  
  // finish errors
  render() {
    const { step } = this.state;
    const { firstName, lastName, email, teamName, major, tShirt, city, firstNameTwo,
    lastNameTwo, emailTwo, teamNameTwo, majorTwo, tShirtTwo, cityTwo,
    firstNameThree, lastNameThree,
    emailThree, teamNameThree, majorThree, tShirtThree, cityThree,
    firstNameFour, lastNameFour, emailFour, teamNameFour,
    majorFour, tShirtFour, cityFour, disabled} = this.state;

    const values = { firstName, lastName, email, teamName, major, tShirt, city, firstNameTwo,
    lastNameTwo, emailTwo, teamNameTwo, majorTwo, tShirtTwo, cityTwo,
    firstNameThree, lastNameThree, emailThree, teamNameThree, majorThree, tShirtThree, cityThree,
    firstNameFour, lastNameFour, emailFour, teamNameFour,
    majorFour, tShirtFour, cityFour, disabled};



    switch (step) {
      case 1:
        return (
          <FormUserDetails
            nextStep={this.nextStep}
            handleChange={this.handleChange}
            values={values}
          />
        );
      case 2:
        return (
          <FormUserDetailsTwo
            nextStep={this.nextStep}
            prevStep={this.prevStep}
            handleChange={this.handleChange}
            values={values}
          />
        );
        case 3:
        return (
          <FormUserDetailsThree
            nextStep={this.nextStep}
            prevStep={this.prevStep}
            handleChange={this.handleChange}
            values={values}
          />
        );
        case 4:
        return (
          <FormUserDetailsFour
            nextStep={this.nextStep}
            prevStep={this.prevStep}
            handleChange={this.handleChange}
            values={values}
          />
        );
      case 5:
        return (
          <Confirm
            nextStep={this.nextStep}
            prevStep={this.prevStep}
            values={values}
          />
        );
      case 6:
        return(
            //this.saveUser(),
            <Success />);
      default:
        (console.log('This is a multi-step form built with React.'))
    }
  }
}

export default UserForm;
