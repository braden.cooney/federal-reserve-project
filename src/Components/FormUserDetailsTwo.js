import React, { Component } from 'react';
import Button from '@material-ui/core/Button';

export class FormUserDetailsTwo extends Component {
  continue = e => {
    e.preventDefault();
    this.props.nextStep();
  };

  back = e => {
    e.preventDefault();
    this.props.prevStep();
  };

  render() {
    const { values, handleChange } = this.props;
    return (

      <div class="form-container">
      <form class="register-form">
        <label htmlFor='team-name'>
       
       Please enter your details below
       </label>
       <input
          id="team-name"
          class="form-field"
          type="text"
          placeholder="Team Name"
          onChange={handleChange('teamNameTwo')}
          defaultValue={values.teamNameTwo}
          name="team-name"
        />
        <label htmlFor='team-name'>
       
       
       </label>
        <input
          id="first-name"
          class="form-field"
          type="text"
          placeholder="First Name"
          onChange={handleChange('firstNameTwo')}
          defaultValue={values.firstNameTwo}
          name="firstName"
        />
        <label htmlFor='team-name'>
       
       
       </label>
       
        <input
          id="last-name"
          class="form-field"
          type="text"
          placeholder="Last Name"
          onChange={handleChange('lastNameTwo')}
          defaultValue={values.lastNameTwo}
          name="lastName"
        />
        <label htmlFor='team-name'>
       
       
       </label>
        <input
          id="email"
          class="form-field"
          type="text"
          placeholder="Email"
          onChange={handleChange('emailTwo')}
          defaultValue={values.emailTwo}
          name="email"
        />
       <label htmlFor='team-name'>
       
      
       </label> 
        <input
          id="major"
          class="form-field"
          type="text"
          placeholder="Major"
          onChange={handleChange('majorTwo')}
          defaultValue={values.majorTwo}
          name="major"
        />
        <label htmlFor='team-name'>
       
       
       </label>
        <input
          id="major"
          class="form-field"
          type="text"
          placeholder="tShirt"
          onChange={handleChange('tShirtTwo')}
          defaultValue={values.tShirtTwo}
          name="tShirt"
        />
        
        <Button
        disabled={values.disabled}
        onClick={this.continue}>continue
        </Button>
        <Button
              color="secondary"
              variant="contained"
              onClick={this.back}
            >Back</Button>
            
      </form>
    </div>

    );
  }
}

export default FormUserDetailsTwo;
