import React, { Component } from 'react';
import Button from '@material-ui/core/Button';


export class FormUserDetails extends React.Component {

 
  
  continue = (e) => {
    e.preventDefault();
    this.props.nextStep();



  };
 
  render() {
    const { values, handleChange } = this.props;
    return (

      <div class="form-container" onsubmit="return validateForm()">
      
      <form class="register-form">
        
       <label htmlFor='team-name'>
       
       Please enter your details below
       </label>
       <input
          id="team-name"
          class="form-field"
          type="text"
          placeholder="Team Name"
          onChange={handleChange('teamName')}
          defaultValue={values.teamName}
          name="team-name"
          font color="red"
        />

        <label htmlFor='first-name'>
       
      
       </label>
        
        <input
          id="first-name"
          class="form-field"
          type="text"
          placeholder="First Name"
          onChange={handleChange('firstName')}
          defaultValue={values.firstName}
          name="firstName"
        

        />
        <label htmlFor='last-name'>
       
       
       </label>
        <input
          id="last-name"
          class="form-field"
          type="text"
          placeholder="Last Name"
          onChange={handleChange('lastName')}
          defaultValue={values.lastName}
          name="lastName"
        />
        <label htmlFor='email'>
       
      
       </label>
        <input
          id="email"
          class="form-field"
          type="text"
          placeholder="Email"
          onChange={handleChange('email')}
          defaultValue={values.email}
          name="email"
        />
        <label htmlFor='major'>

       
       
       </label>
        <input
          id="major"
          class="form-field"
          type="text"
          placeholder="Major"
          onChange={handleChange('major')}
          defaultValue={values.major}
          name="major"
        />
        <label htmlFor='last-name'>
       
       
       </label>
        <input
          id="tShirt"
          class="form-field"
          type="text"
          placeholder="tShirt"
          onChange={handleChange('tShirt')}
          defaultValue={values.tShirt}
          name="tShirt"
        />
        <Button
        disabled={values.disabled}
        onClick={this.continue}>continue
        </Button>
          
          

      </form>
    </div>


    );

  }

}


export default FormUserDetails;

