import React, { Component } from 'react';
import Button from '@material-ui/core/Button';

export class FormUserDetailsThree extends Component {
  continue = e => {
    e.preventDefault();
    this.props.nextStep();
  };

  back = e => {
    e.preventDefault();
    this.props.prevStep();
  };

    render() {
    const { values, handleChange } = this.props;
    return (

      <div class="form-container">
      <form class="register-form">
      <label htmlFor='team-name'>
       
       Enter an appropriate Team Name
       </label>
       <input
          id="team-name"
          class="form-field"
          type="text"
          placeholder="Team Name"
          onChange={handleChange('teamNameThree')}
          defaultValue={values.teamNameThree}
          name="team-name"
        />
        <label htmlFor='team-name'>
       
       Enter your first name
       </label>
        <input
          id="first-name"
          class="form-field"
          type="text"
          placeholder="First Name"
          onChange={handleChange('firstNameThree')}
          defaultValue={values.firstNameThree}
          name="firstName"
        />
        <label htmlFor='team-name'>
       
       Enter a last name
       </label>
        <input
          id="last-name"
          class="form-field"
          type="text"
          placeholder="Last Name"
          onChange={handleChange('lastNameThree')}
          defaultValue={values.lastNameThree}
          name="lastName"
        />
       <label htmlFor='team-name'>
       
       Enter your email
       </label>
        <input
          id="email"
          class="form-field"
          type="text"
          placeholder="Email"
          onChange={handleChange('emailThree')}
          defaultValue={values.emailThree}
          name="email"
        />
        <label htmlFor='team-name'>
       
       Enter your major
       </label>
        <input
          id="major"
          class="form-field"
          type="text"
          placeholder="Major"
          onChange={handleChange('majorThree')}
          defaultValue={values.majorThree}
          name="major"
        />
        <label htmlFor='team-name'>
       
       Enter your major
       </label>
       <label htmlFor='team-name'>
       
       Enter your tshirt size
       </label>
        <input
          id="major"
          class="form-field"
          type="text"
          placeholder="tShirt"
          onChange={handleChange('tShirt')}
          defaultValue={values.tShirtThree}
          name="tShirt"
        />
        <Button
        disabled={values.disabled}
        onClick={this.continue}>continue
        </Button>
        <Button
              color="secondary"
              variant="contained"
              onClick={this.back}
            >Back</Button>
      </form>
    </div>

    );
  }
}

export default FormUserDetailsThree;
