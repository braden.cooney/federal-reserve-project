import React, { Component } from 'react';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import { ThemeProvider as MuiThemeProvider } from '@material-ui/core/styles';
import { List, ListItem, ListItemText } from '@material-ui/core';
import Button from '@material-ui/core/Button';

import {API, graphqlOperation} from 'aws-amplify';
import {createForm} from '../graphql/mutations';
import awsconfig from '../aws-exports';
import Amplify, {Auth} from 'aws-amplify';


export class Confirm extends Component {
  constructor(props) {
    super(props)
    this.state = {
      username: ''
      , firstName: 'Braden', lastName: 'Cooney', email: 'email', major: 'Computer Science', teamName: 'UCM', tshirt: 'large', id: '2'
    }
  }

  continue = e => {
    e.preventDefault();

    // eslint-disable-next-line no-unused-expressions
    this.saveForm(),
    this.props.nextStep();
  };

  saveForm = async () => {
    console.log("we got here");
    //const input = this.props.Input;
    const {firstName, lastName, email, major, teamName, tShirt} = this.props.values
    const id = Date.now() * Math.floor(Math.random() * 10)
    const tshirt = tShirt
    const form = {firstName, lastName, email, major, teamName, tshirt, id}
    try {
      await API.graphql(graphqlOperation(createForm, {input: form}))
      console.log("Success")
    } catch (err) {
      console.log(err)
    }
  }

  back = e => {
    e.preventDefault();
    this.props.prevStep();
  };




  render() {
    const {
      values: { firstName, lastName, email, teamName, major, tShirt, city, firstNameTwo,
    lastNameTwo, emailTwo, teamNameTwo, majorTwo, tShirtTwo,
    firstNameThree, lastNameThree,
    emailThree, teamNameThree, majorThree, tShirtThree,
    firstNameFour, lastNameFour, emailFour, teamNameFour,
    majorFour, tShirtFour }
    } = this.props;
    return (
      <MuiThemeProvider>
        <>
          <Dialog
            open
            fullWidth
            maxWidth='sm'
          >

            <AppBar title="Confirm User Data" />
            <List>
              <ListItem>
                <ListItemText primary="First Name" secondary={firstName} />
              </ListItem>
              <ListItem>
                <ListItemText primary="Last Name" secondary={lastName} />
              </ListItem>
              <ListItem>
                <ListItemText primary="Email" secondary={email} />
              </ListItem>
              <ListItem>
                <ListItemText primary="City" secondary={city} />
              </ListItem>
              <ListItem>
                <ListItemText primary="Team Name" secondary={teamName} />
              </ListItem>
              <ListItem>
                <ListItemText primary="Major" secondary={major} />
              </ListItem>
              <ListItem>
                <ListItemText primary="T-shirt" secondary={tShirt} />
              </ListItem>
            </List>
            <br />

          

            <Button
              color="secondary"
              variant="contained"
              onClick={this.back}
            >Back</Button>

            <Button
              color="primary"
              variant="contained"
              onClick={this.continue}
            >Confirm & Continue</Button>
          </Dialog>
        </>
      </MuiThemeProvider>
    );
  }
}

export default Confirm;
