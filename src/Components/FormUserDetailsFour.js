import React, { Component } from 'react';
import Button from '@material-ui/core/Button';

export class FormUserDetailsFour extends Component {
  continue = e => {
    e.preventDefault();
    this.props.nextStep();
  };

  back = e => {
    e.preventDefault();
    this.props.prevStep();
  };

    render() {
    const { values, handleChange } = this.props;
    return (

      <div class="form-container">
      <form class="register-form">
      <label htmlFor='team-name'>
       
       
       </label>
       <input
          id="team-name"
          class="form-field"
          type="text"
          placeholder="Team Name"
          onChange={handleChange('teamNameFour')}
          defaultValue={values.teamNameFour}
          name="team-name"
        />
        <label htmlFor='team-name'>
       
       
       </label>
        <input
          id="first-name"
          class="form-field"
          type="text"
          placeholder="First Name"
          onChange={handleChange('firstNameFour')}
          defaultValue={values.firstNameFour}
          name="firstName"
        />
        <label htmlFor='team-name'>
       
       
       </label>
        <input
          id="last-name"
          class="form-field"
          type="text"
          placeholder="Last Name"
          onChange={handleChange('lastNameFour')}
          defaultValue={values.lastNameFour}
          name="lastName"
        />
        <label htmlFor='team-name'>
       
       
       </label>
        <input
          id="email"
          class="form-field"
          type="text"
          placeholder="Email"
          onChange={handleChange('emailFour')}
          defaultValue={values.emailFour}
          name="email"
        />
        <label htmlFor='team-name'>
       
       
       </label>
        <input
          id="major"
          class="form-field"
          type="text"
          placeholder="Major"
          onChange={handleChange('majorFour')}
          defaultValue={values.majorFour}
          name="major"
        />
        <label htmlFor='team-name'>
       
       
       </label>
        <input
          id="major"
          class="form-field"
          type="text"
          placeholder="tShirt"
          onChange={handleChange('tShirt')}
          defaultValue={values.tShirtFour}
          name="tShirt"
        />
         <Button
        disabled={values.disabled}
        onClick={this.continue}>continue
        </Button>
        <Button
              color="secondary"
              variant="contained"
              onClick={this.back}
            >Back</Button>

      </form>
    </div>

    );
  }
}

export default FormUserDetailsFour;
